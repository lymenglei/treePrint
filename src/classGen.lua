local classGen = {}


function classGen.getFindPos(str, pattern)
	local i, j = string.find(str, "function i3k_sbean."..pattern)
	return i, j
end

function classGen.getClassString(str, pattern)
	local i, j = classGen.getFindPos(str, pattern)
	if not i then
		return ""
	end
	assert(i ~= nil, "classGen.getClassString, can't find ["..pattern.."]")
	local endPos = string.find(str, "\nend?", i)
	if endPos then
		return string.sub(str, i, endPos+3)
	end
end

-- �Ե����������з���������strΪgetClassString���ص��ַ���
function classGen.getClassTab(str, pattern)
	local temp = {}
	local fields = utils.splitString(str, "\n")
	assert(#fields > 2, "src split result error. str:"..str)
	for i = 2, #fields - 1 do
		-- delete "self." Space and "-" and so on.
		local patterns = {"-", "self.", " ", "\t"} -- ��Ҫ�滻�ɿհ�
		temp[#temp + 1] = utils.gsubString(fields[i], patterns, "")
	end

	local result = {}
	result.name = pattern
	result.element = {}

	local compValues = {}
	for k, v in ipairs(temp) do
		local keyType = utils.splitString(v, ":")
		result.element[#result.element + 1] = {key = keyType[1], valueType = keyType[2]}
		--[[
			���������������������⣬����keyType[2]Ϊһ���������͵�ʱ����checkFinalType�������ص���false
			vector[int32]
			map[int32,int32]
		--]]
		if not checkFinalType(keyType[2]) then
			table.insert(compValues, keyType[2])
		end
	end
	result.children = {}
	for k, v in ipairs(compValues) do
		local typeValue = handleCompTypeValue(v) -- error set[in32] it return int32; this is final type
		result.children[typeValue] = classGen.getClass(g_readText, typeValue)
	end

	return result
end

-- strΪԭʼ�������ļ���patternΪƥ��������
function classGen.getClass(src, pattern)
	local str = classGen.getClassString(src, pattern)
	assert(str ~= nil, "getClass error, get a nil value")
	return classGen.getClassTab(str, pattern)
end



return classGen
