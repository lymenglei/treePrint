local testCase = {}

local testFindPattern
local testCtorClass

local tab = 
{
	name = "PayRebateReturnConf",
	element = 
	{
		{key = "payReq" , valueType = "int32"},
		{key = "dayReturn" , valueType = "int32"},
		{key = "returnDays" , valueType = "int32"},
		{key = "returnDays" , valueType = "vector[DummyGoods]"},
	},
	children =  -- 递归的数据结构
	{
		DummyGoods = {
			name = "DummyGoods",
			element = 
			{
				{ key = "id", valueType = "int32"},
				{key = "count", valueType = "int32"}, 
			},
			children = 
			{
			}
		},
	}
}

function testCase.testUtils()
	utils.setFileName("input.txt")
	local text = utils.read()
	g_readText = text
	testCase.testGetPatternClass(text, "PayRebateReturnConf") -- main TestCase
end
function testCase.test_i3k_Sbean(string)
	utils.setFileName("i3k_sbean.lua")
	local text = utils.read()
	g_readText = text
	testCase.testGetPatternClass(text, string) -- main TestCase
end

function testCase.testWorkingPath()
	-- io 文件目录在工程 根目录
	local file = io.open("bbb.txt","w")
	io.output(file)
	io.write("bbbbbb")
	io.close(file)
end

-------------------------
-- 匹配一个字符串，返回找到的位置
function testCase.testFindPattern(str, pattern)
	local i, j = classGen.getFindPos(str, pattern)
	print(i, j)
	
	local endPos = string.find(str, "\nend?", i)
	print(string.sub(str, i, endPos+3))
	print("========================================")
end

-- 返回找到的类的N行
function testCase.testGetPatternClass(str, pattern)
	local result = classGen.getClassString(str, pattern)
	testCase.testCtorClass(result, pattern)
end

-- 根据返回的N行，构造一个对象,return table
function testCase.testCtorClass(str, pattern)
	local tab = classGen.getClassTab(str, pattern)
	outPut.printTree(tab)
end


---------------------------------
-- test output
function testCase.testOutPut(tab)
	outPut.printTable(tab)
end

function testCase.testParseDataStruct()
	local vec = handleVector("vector[DummyGoods]")
	assert(vec[1] == "DummyGoods", "vector parse error")
	
	local set = handleSet("set[DummyGoods]")
	assert(set[1] == "DummyGoods", "set parse error")
	
	-- input string should not hava space,在生成对象字符串时候已经将空格去掉了
	local map = handleMap("map[int32,DBGoodsGroupBuyLog]")
	assert(map[1] == "int32" and map[2] == "DBGoodsGroupBuyLog", "map parse error")
	
	-- 先不考虑这种结构
--	local mapKey2, mapValue2 = handleMap("map[int32,map[int32,DBGoodsGroupBuyLog]]")
--	print( mapKey2, mapValue2 )
--	assert(mapKey2 == "int32" and mapValue2 == "DBGoodsGroupBuyLog", "map parse error")
end

function testCase.testPrintTree(tab)
	outPut.printTree(tab)
end

return testCase