local utils = {}

utils.fileName = nil

local close

function utils.setFileName(name)
	utils.fileName = name
end

function utils.getFileName()
	return utils.fileName
end


function utils.readFile(name)
	local file = io.open(name, "r")
	local context =  file:read("*a")
	io.close(file)
	return context
end

function utils.writeFile(fileName, contextString)
	local file = io.open(fileName, "a") -- 以附加的方式打开只写文件
	io.output(file) 
	io.write(contextString)
	io.close(file)
end

function utils.clearFile(fileName)
	local file = io.open(fileName, "w") 
	io.output(file) 
	io.write("")
	io.close(file)
end

function utils.read()
	local fileName = utils.getFileName()
	assert(fileName ~= nil, "befor use utils.read(), you need to set fileName");
	return utils.readFile(fileName)
end

function utils.close()
	local fileName = utils.getFileName()
	assert(fileName ~= nil, "befor use utils.read(), you need to set fileName");
	io.close(fileName)
end

-------------------------------------

-- 字符串分割 string.split
function utils.splitString(src, sep)
	local sep, fields = sep or "\t", {}  
    local pattern = string.format("([^%s]+)", sep)  
    string.gsub(src, pattern, function(c) fields[#fields+1] = c end)  
    return fields  
end

-- 替换str中，配置patterns的部分，替换为target
function utils.gsubString(src, patterns, target)
	for k, v in ipairs(patterns) do
		src = string.gsub(src, v, target)
	end
	return src
end

--------------------------------

function utils.print(string)
	print(string)
	utils.writeFile("output.txt", string.."\n")
end

return utils