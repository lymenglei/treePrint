local init = {}

function init.init_env()
	classGen = require("src/classGen")
	define = require("src/define")
	outPut = require("src/outPut")
	testCase = require("src/testCase")
	utils = require("src/utils")
	g_readText = nil
end

function init.initCfg()
	utils.setFileName("i3k_sbean.lua")
	local text = utils.read()
	g_readText = text
end

function init.findStr(pattern)
	local result = classGen.getClassString(g_readText, pattern)
	local tab = classGen.getClassTab(result, pattern)
	outPut.printTree(tab)
end

return init

