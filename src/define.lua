

--[[
	2018.1.27 
	写到了一半，发现不能以模块的形式一直写，有的功能需要嵌套require，这样就会造成一些问题，
	所以需要改一下结构，将常用的模块require到一个全局的table中？
	当未来模块越来越多的时候，就不能采用这种方式进行调用了。此时需要重构
]]

TYPE_VEC = 1
TYPE_SET = 2
TYPE_MAP = 3
TYPE_STRUCT = 4


FINAL_TYPE = 
{
	-- TODO fix it set[int32] 2018.4.11
	"int32", "string", "int8", "int16", "float", "set[int32]" 
}


-- args: etc vector[DummyGoods]
-- return a list
function handleVector(str)
	local field = string.gsub(str, "vector", "")
	field = string.sub(field, 2, string.len(field) - 1)
	return {field}
end

function handleSet(str)
	local field = string.gsub(str, "set", "")
	field = string.sub(field, 2, string.len(field) - 1)
	return {field}
end

-- map[int32,DBGoodsGroupBuyLog] 输入字符串无空格，先不考虑嵌套的map结构，
function handleMap(str)
	local type = string.gsub(str, "map", "")
	type = string.sub(type, 2, string.len(type) - 1)
	local fields = utils.splitString(type, ",")
	return fields
end

function handleStruct(str)
	return {str}
end

COMP_TYPE = 
{
	[TYPE_VEC] = { name = "vector", func = handleVector},
	[TYPE_SET] = { name = "set", func = handleSet},
	[TYPE_MAP] = { name = "map", func = handleMap},
	[TYPE_STRUCT]= { name = "struct", func = handleStruct},
}

--[[
	这里检查最终类型有问题，对于keyType[2]为一下两种类型的时候，checkFinalType函数返回的是false
	
	- vector[int32]
	- map[int32,int32]
	
--]]

function reCheckFinalType(string)
	local type = checkCompType(string)
	
	
	if type == TYPE_VEC then
		local t = string.gsub(string, "vector", "")
		t = string.sub(t, 2, string.len(t) - 1)
		return checkFinalType(t)
	elseif type == TYPE_SET then
		local t = string.gsub(string, "set", "")
		t = string.sub(t, 2, string.len(t) - 1)
		return checkFinalType(t)
	elseif type == TYPE_MAP then
		local t = string.gsub(string, "map", "")
		t = string.sub(t, 2, string.len(t) - 1)
		local list = utils.splitString(t, ",")
		local is = true
		for k, v in ipairs(list) do
			local c = checkFinalType(v)
			is = is and c
		end
		return is
	else -- TYPE_STRUCT
		return false
	end
end

function checkFinalType(type)
	local tab = FINAL_TYPE
	for k, v in ipairs(tab) do
		if v == type then
			return true
		else
--			 print("["..type.."] not equal to ["..v.."]")	
		end
	end
	return reCheckFinalType(type)
--	return false
end


-- args: etc vector[DummyGoods]
function checkCompType(str)
	local head = string.sub(str, 1, 3)
	if head == "vec" then
		return TYPE_VEC
	elseif head == "set" then
		return TYPE_SET
	elseif head == "map" then
		return TYPE_MAP
	else
		return TYPE_STRUCT
	end
end


function getCompTypeFunc(typeID)
	return COMP_TYPE[typeID].func
end


function handleDataStruct(typeID, fields)
	if typeID == TYPE_VEC then
		local tab = classGen.getClass(g_readText, fields[1]) --需要保存下全局读取的输入文件
		outPut.printTable(tab)
	elseif typeID == TYPE_SET then
		local tab = classGen.getClass(g_readText, fields[1])
		outPut.printTable(tab)
	elseif typeID == TYPE_MAP then
		local tab = classGen.getClass(g_readText, fields[2])
		outPut.printTable(tab)
	elseif type == TYPE_STRUCT then
		local tab = classGen.getClass(g_readText, fields[1])
		outPut.printTable(tab)
	end
end

-- type = set[DBRolePayRebateLog]
function handleCompType(type)
	local typeID = checkCompType(type)
	local func = getCompTypeFunc(typeID)
	local fields = func(type)
	return handleDataStruct(typeID, fields)
end

-- type = set[DBRolePayRebateLog]   -- type = set[int32] error
function handleCompTypeValue(type)
	local typeID = checkCompType(type)
	local func = getCompTypeFunc(typeID)
	local fields = func(type)
	if typeID == TYPE_VEC then
		return fields[1]
	elseif typeID == TYPE_SET then
		return fields[1]
	elseif typeID == TYPE_MAP then
		return fields[2], fields[1]
	elseif typeID == TYPE_STRUCT then
		return fields[1]
	end
end

-- type = set[DBRolePayRebateLog]
function getCompType(type)
	local typeID = checkCompType(type)
	local func = getCompTypeFunc(typeID)
	local fields = func(type)
	return COMP_TYPE[typeID].name, fields[1]
end
