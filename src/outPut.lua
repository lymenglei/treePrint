local outPut = {}

-- table and depth
function outPut.getDepthSep(depth, sepList)
 	local str = ""
 	local func = function(index)
 		for k, v in pairs(sepList or {}) do
 			if k == index then
 				return true
 			end
 		end
 		return false
 	end
	for i = 1, depth do
		local sep = func(i) and "│" or ""
		str = str .. "    "..sep
	end
	return str
end

function outPut.getSepSymble(isLast)
	return isLast and "└──" or "├──"
end


-- 横向递归打印树型结构的tab
function outPut.printTree(tab, depth, sepList)
	depth = depth or 1
	local sep = outPut.getDepthSep(depth, sepList)
	if depth == 1 then
		utils.print("")
		utils.print(tab.name)
	end

	for k, v in ipairs(tab.element)do
		local symble = outPut.getSepSymble(k == #tab.element)

		-- 处理复杂的数据结构中，嵌套的问题
		local listT = sepList or {} -- 传递引用，模拟栈
		if k ~= #tab.element then
			listT[depth] = true
		elseif k == #tab.element then
			listT[depth] = nil
		end

		if checkFinalType(v.valueType) then
			utils.print(sep..symble..v.key.." ("..v.valueType..")")
		else
			local valueName = handleCompTypeValue(v.valueType)
			local structName, arg2 = getCompType(v.valueType)
			if structName == "map" and arg2 then
				utils.print(sep..symble..v.key.." map["..arg2..", "..valueName.."]")
				utils.print(sep.."   "..valueName)
			else
				utils.print(sep..symble..v.key.." "..valueName.."("..structName..")")
			end
			outPut.printTree(tab.children[valueName], depth + 1, listT)
		end
	end
end


return outPut
